----------------------------------------------------------------------

  solution "musedriver"

    language "C++"
    location ( _OPTIONS["to"] or _ACTION )


    includedirs {
    }

    -- define all the possible build configurations
    configurations {
      "Debug", "Release"
    }
    
    
    
  project "musedriver"

    kind     "ConsoleApp"
    location ( _OPTIONS["to"] or _ACTION )
    links   { "user32",
              "wsock32",
              "ws2_32",
              "iphlpapi",
              "pthreadVC2",
              "liblo"
            }

    files {
    	"../src/musedriver.cpp",
    	"../src/nsutil.c",
    	"../src/openedf.c",
    	"../src/nsnet.c",
    	"../src/monitor.c"
    }

    
    
