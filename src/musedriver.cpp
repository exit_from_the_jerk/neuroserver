#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <functional>
#include <math.h>
#include <assert.h>
#include <string.h>


extern "C" {
#include "nsutil.h"
#include "nsnet.h"
#include "nsser.h"
#include "openedf.h"
}

#define WIN32
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <lo/lo.h>



using namespace std;
using boost::property_tree::ptree;
using boost::property_tree::read_json;





/* This is the maximum size of a protocol packet */
#define PROTOWINDOW 24

#define MAXPACKETSIZE 17

char buf[PROTOWINDOW];
sock_t sock_fd = -1;
int bufCount;
static int failCount = 0;
static struct OutputBuffer ob;
static struct InputBuffer ib;

const char *hostname = NULL;
unsigned short portno = 0;

bool got_config = false;
ptree config_pt;


int hasMatchedProtocol;
int isP2, isP3;
int isValidPacket(unsigned short nchan, unsigned short *samples);
int doesMatchP3(unsigned char c, unsigned short *vals,int *nsamples);
int doesMatchP2(unsigned char c, unsigned short *vals,int *nsamples);
int mGetOK(sock_t fd, struct InputBuffer *ib);

int mGetOK(sock_t fd, struct InputBuffer *ib)
{
	return 0;
}


/* filled out with some defaults, updated by the first config message */
static struct EDFDecodedConfig modEEGCfg = {
		{ 0,   // header bytes, to be set later
			-1,  // data record count
			4,   // channels
			"0", // data format
			"Muser",
			"the obMuser",
			"date",
			"time",
			"Interaxon",
			1.0,  // 1 second per data record
		}, {
				{
					220, // samples per second
					-842, 842, // physical range
					0, 1023,   // digital range
					"A1", // label
					"metal electrode",  // transducer
					"uV",  // physical unit dimensions
					"LP:59Hz", // prefiltering
					""         // reserved
				},
				{
					220, // samples per second
					-842, 842, // physical range
					0, 1023,   // digital range
					"FP1", // label
					"metal electrode",  // transducer
					"uV",  // physical unit dimensions
					"LP:59Hz", // prefiltering
					""         // reserved
				},
				{
					220, // samples per second
					-842, 842, // physical range
					0, 1023,   // digital range
					"FP2", // label
					"metal electrode",  // transducer
					"uV",  // physical unit dimensions
					"LP:59Hz", // prefiltering
					""         // reserved
				},
				{
					220, // samples per second
					-842, 842, // physical range
					0, 1023,   // digital range
					"A2", // label
					"metal electrode",  // transducer
					"uV",  // physical unit dimensions
					"LP:59Hz", // prefiltering
					""         // reserved
				},
		}
	};

static struct EDFDecodedConfig current;


void handleSamples(int packetCounter, int chan, unsigned short *vals)
{
	if (sock_fd >= 0) {
		char buf[MAXLEN];
		int bufPos = 0;
		int i;
	//	printf("Got good packet with counter %d, %d chan\n", packetCounter, chan);
		if (chan > current.hdr.dataRecordChannels)
			chan = current.hdr.dataRecordChannels;
		bufPos += sprintf(buf+bufPos, "! %d %d", packetCounter, chan);
		for (i = 0; i < chan; ++i)
			bufPos += sprintf(buf+bufPos, " %d", vals[i]);
		bufPos += sprintf(buf+bufPos, "\r\n");
		writeString(sock_fd, buf, &ob);
		mGetOK(sock_fd, &ib);
	}
}




void
osc_server_error(int /* num */, const char *m, const char *path) {
	cout << "osc server error: " << m << " path: " << path << "\n";
}


int
osc_generic_handler(const char *path, const char * types, lo_arg ** /* argv */,
                    int /* argc */, void * /* data */, void * /* user_data */) {

	// cout << "osc server unrecognized message: " << path << " " << types << "\n";

	return 0;
}



static int osc_packet_counter = 0;

static string most_recent_preset("");

/* keys for config paths copied from ixcloud muse_v2.proto */
#define PRESET				"preset"
#define MAC_ADDR			"mac_addr"
#define FILTERS_ENABLED		"filters_enabled"
#define ACCEL_ENABLED		"accelerometer_data_enabled"
#define NOTCH_FREQ			"notch_frequency_hz"
#define BATT_STAT_ENABLED   "battery_data_enabled"
#define ERR_STAT_ENABLED    "error_stat_enabled"
#define COMPRESSION_ENABLED "compression_enabled"
#define EEG_SAMPLE_FREQ		"eeg_sample_frequency_hz"
#define EEG_OUTPUT_FREQ		"eeg_output_frequency_hz"
#define EEG_CHAN_COUNT		"eeg_channel_count"
#define EEG_CHAN_BITS		"eeg_samples_bitwidth"
#define EEG_CHAN_LAYOUT		"eeg_channel_layout"
#define DOWNSAMPLE			"eeg_downsample"
#define BATT_REMAIN			"battery_percent_remaining"
#define BATT_MV				"battery_millivolts"
#define AFE_GAIN			"afe_gain"
#define EEG_UNITS			"eeg_units"
#define ACC_UNITS			"acc_units"
#define ACC_FREQ			"acc_sample_frequency_hz"
#define SCALE_FACTOR_EEG    "eeg_conversion_factor"
#define SCALE_FACTOR_ACCEL  "acc_conversion_factor"
#define SCALE_FACTOR_DRL	"drlref_conversion_factor"
#define SERIAL_NUM			"serial_number"
#define BAT_PKTS_ENABLED	"battery_data_enabled"
#define ACC_PKTS_ENABLED	"acc_data_enabled"
#define DRLREF_PKTS_ENABLED	"drlref_data_enabled"
#define DRLREF_FREQ			"drlref_sample_frequency_hz"
#define ERR_PKTS_ENABLED	"error_data_enabled"

/* keys for version message */
#define HW_VER 			"hardware_version"
#define FW_VER 			"firmware_version"
#define FW_TYPE			"firmware_type"
#define BT_FW_VER       "firmware_bootloader_version"
#define BUILD_NO		"build_number"
#define PROTO_VER       "protocol_version"



static void
init_osc_to_edf_bridge( ptree &pt ) {
	char EDFPacket[MAXHEADERLEN];
	int EDFLen = MAXHEADERLEN;

	modEEGCfg.hdr.dataRecordChannels = pt.get<int>(EEG_CHAN_COUNT);

    time_t now;
    time( &now );
    struct tm new_time;
	localtime_s(&new_time, &now);
    strftime( modEEGCfg.hdr.recordingStartDate,
    		  sizeof(modEEGCfg.hdr.recordingStartDate),
    		  "%d.%m.%y", &new_time );

    strftime( modEEGCfg.hdr.recordingStartDate,
    		  sizeof(modEEGCfg.hdr.recordingStartTime),
    		  "%H.%M.%S", &new_time );

    strcpy( modEEGCfg.hdr.localRecorder, pt.get<string>( MAC_ADDR ).c_str() );

	string labels = pt.get<string>(EEG_CHAN_LAYOUT).c_str();

	const char *chan_labels = labels.c_str();

	for (int ch = 0; ch < modEEGCfg.hdr.dataRecordChannels; ch += 1) {

		memset(modEEGCfg.chan[ch].label, 0, sizeof(modEEGCfg.chan[ch].label));

		if (chan_labels[2] == ' ' || chan_labels[2] == '\0') {
			memcpy(modEEGCfg.chan[ch].label, chan_labels, 2);
			chan_labels += 3;
		}
		else {
			memcpy(modEEGCfg.chan[ch].label, chan_labels, 3);
			chan_labels += 4;
		}

		modEEGCfg.chan[ch].sampleCount = pt.get<int>(EEG_OUTPUT_FREQ);

		int bits = pt.get<int>(EEG_CHAN_BITS);

		modEEGCfg.chan[ch].digiMax = (1 << bits) - 1;
		modEEGCfg.chan[ch].digiMin = 0;

		double eeg_scale = pt.get<double>(SCALE_FACTOR_EEG);

		modEEGCfg.chan[ch].physMax = eeg_scale * (modEEGCfg.chan[ch].digiMax/2);
		modEEGCfg.chan[ch].physMin = -eeg_scale * (modEEGCfg.chan[ch].digiMax/2);

		strcpy(modEEGCfg.chan[ch].dimUnit, "uV");
	}

	makeREDFConfig(&current, &modEEGCfg);
	writeEDFString(&current, EDFPacket, &EDFLen);

	int fd = rsocket();
	if (fd < 0) {
		perror("socket");
	}
	else {
		setblocking(fd);

		rprintf("Attempting to connect to nsd at %s:%d\n", hostname, portno);
		int retval = rconnectName(fd, hostname, portno);
		if (retval != 0) {
			rprintf("connect error\n");
			rexit(1);
		}
		rprintf("Socket connected.\n");
		fflush(stdout);

		writeString(fd, "eeg\n", &ob);
		mGetOK(fd, &ib);
		writeString(fd, "setheader ", &ob);
		writeBytes(fd, EDFPacket, EDFLen, &ob);
		writeString(fd, "\n", &ob);
		mGetOK(fd, &ib);
		updateMaxFd(fd);

		// enable eeg data handler to pass through data
		sock_fd = fd;
	}
}


static int
osc_config_handler( const char * /* path */, const char *types, lo_arg ** argv,
               	 int argc, void * /* data */, void * /* user_data */) {

	static string last_preset;

	if (argc && types[0] == 's') {
		
		string json;

		json += ((char *)(&argv[0]->s));

		ptree pt;
		std::istringstream is(json);
		read_json(is, pt);

		string cur_preset = pt.get<string>(PRESET); 

		if (cur_preset.compare(last_preset) != 0) {

			last_preset = cur_preset;

			if (sock_fd >= 0)
				::closesocket(sock_fd);

			cout << "Creating EDF stream derived from /muse/config message." << endl;

			init_osc_to_edf_bridge(pt);
		}
	}

	return 0;
}


int
osc_eeg_handler( const char * /* path */, const char *types, lo_arg ** argv,
               	 int argc, void * /* data */, void * /* user_data */) {
	if (argc >= 4 && argc < 10) {

		unsigned short vals[10] = { 0,0,0,0,0,0,0,0,0,0 };

		for (int i = 0; i < argc; i += 1) {
			if (types[i] == 'i') {
				vals[i] = (unsigned short)(argv[i]->i);
			} else if (types[i] == 'f') {
				vals[i] = (unsigned short)round( argv[i]->f );
			}
		}

		handleSamples(osc_packet_counter++, modEEGCfg.hdr.dataRecordChannels, &vals[0]);
	}

    return 0;
}




int main(int argc, char **argv)
{
	cout << "Muse OSC-to-EDF bridge\n";
	cout << "\n\n";

	const char *usage =
			    "museosc2edf <osc input port> <nsd hostname> <nsd port>\n"
			    "\t<osc input port>\twhat port to listen for osc messages on, default " DEFAULTOSC_IN_PORT "\n"
				"\t<nsd hostname>  \tneuroserver host, default " DEFAULTHOST "\n"
				"\t<nsd port>      \tneuroserver port, default 8336\n"
				;

    const char *osc_port = NULL;
    char responseBuf[MAXLEN];
	
	rinitNetworking();

	for (int i = 1; argv[i]; ++i) {
		if (argv[i][0] == '-' && argv[i][1] == 'h') {
			cout << usage;
			exit(0);
		}

		if (osc_port == NULL) { 
			osc_port = argv[i];
			continue;
		}

		if (hostname == NULL) {
			hostname = argv[i];
			continue;
		}

		if (portno == 0) {
			portno = atoi(argv[i]);
			continue;
		}
	}

	if (portno == 0)
		   portno = DEFAULTPORT;
	if (hostname == NULL)
		   hostname = DEFAULTHOST;
	if (osc_port == NULL)
		   osc_port = DEFAULTOSC_IN_PORT;


	/*
	 * setup a new OSC server to listen for signals
	 */
	lo_server osc_server = 0;

	osc_server = lo_server_new(osc_port, (lo_err_handler)osc_server_error);

	if (osc_server != 0) {

		lo_server_add_method(osc_server, "/muse/eeg",
			NULL, osc_eeg_handler, (void *)osc_server );

		lo_server_add_method(osc_server, "/muse/config",
			"s", osc_config_handler, (void *)osc_server );

		lo_server_add_method(osc_server, NULL,
			NULL, osc_generic_handler, (void *)osc_server );

	} else {
		cout << "Error allocating OSC server!" << endl;
		exit(-1);
	}


	int osc_fd = lo_server_get_socket_fd(osc_server);

	for (;;) {

		fd_set toread;

		FD_ZERO(&toread);

		if (sock_fd != (unsigned)-1)
			FD_SET(sock_fd, &toread);

		FD_SET(osc_fd, &toread);

		struct timeval when;
		when.tv_sec = 0;
		when.tv_usec = (500000L);

		int retval = rselect(max_fd, &toread, NULL, NULL);

		if (sock_fd >= 0 && FD_ISSET(sock_fd, &toread)) {
			my_read(sock_fd, responseBuf, MAXLEN, &ib);
		}

		if (FD_ISSET(osc_fd, &toread)) {
			lo_server_recv_noblock(osc_server, 0);
		}

		if (isEOF(sock_fd, &ib)) {
			rprintf("Server died, exiting.\n");
			exit(0);
		}
	}

	return 0;
}

